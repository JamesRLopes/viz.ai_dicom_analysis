#!/usr/bin/python

import os,glob,urllib,sys,pydicom,tarfile

inputURL = sys.argv[1]


print "--Downloading file!\n"
# Grab file
urllib.urlretrieve (inputURL, "DICOM.tgz")

print "--Extracting file!\n"
# Extract contents
targz = tarfile.open("DICOM.tgz", "r:gz")
targz.extractall()
targz.close

# analyze patient data

unique_list=[]

print "Results: \n"
#get unique entries
def getUniq(list):
	list_all = (set(list.split(",")))
      	for entry in list_all:
	        if entry not in unique_list:
        	        unique_list.append(entry)
        return unique_list


for dcmfile in glob.glob("*.dcm"):
	ds = pydicom.dcmread(dcmfile)
	pName=(ds.PatientName)
	pAge=(ds.PatientAge)
	pSex=(ds.PatientSex)
	values = ("Name: ",pName," Age: ",pAge," Sex: ",pSex)
	patientInfo = ''.join(values)

	getUniq(patientInfo)

#output only the unique patient info
for x in range(len(unique_list)):
	print unique_list[x]
unique_list=[]
for dcmfile in glob.glob("*.dcm"):
	ds=pydicom.dcmread(dcmfile)
        pInstitution=(ds.InstitutionName)
	getUniq(''.join(pInstitution))
print "\nNumber of Hospitals: ", len(set(unique_list))

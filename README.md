######################### 
######################### 
######## READ ME ######## 
######################### 
######################### 

__Note: Users must have the pydicom module installed__

This script takes a single arguement which must be a URL to a tar.gz formatted file containing DICOM images for analysis

####### Example run command: #######

$ python viz_DICOM.py  https://s3.amazonaws.com/viz_data/DM_TH.tgz

The output contains the name, age and sex of the patient as well as how many source institutions the files came from

####### Example output: ############
###
### --Downloading file!
###
### --Extracting file!
###
### Results: 
###
### Name: 1.2.840.113619.2.337.3.2831186181.801.1414550448.623 Age: 063Y Sex: F
### Name: 1.3.12.2.1107.5.1.4.0.30000016080509452051500000829 Age: 032Y Sex: M
### Name: 1.3.12.2.1107.5.1.4.0.30000016082001341060900000787 Age: 029Y Sex: M
### Name: 1.2.840.113619.2.337.3.2831186181.442.1421722000.421 Age: 011Y Sex: M
### Name: 1.2.840.113619.2.337.3.2831186181.704.1420253349.16 Age: 060Y Sex: F
### Name: 1.3.12.2.1107.5.1.4.0.30000016082001341060900000829 Age: 060Y Sex: F
###
### Number of Hospitals:  3

